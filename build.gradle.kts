plugins {
    id("org.jetbrains.kotlin.jvm").version("1.9.0")
    id("com.gradle.plugin-publish") version "1.2.0"
}

group = "com.appspot.magtech"
version = "0.4"

repositories {
    mavenCentral()
}

dependencies {
    compileOnly(gradleApi())
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

tasks.named<Jar>("jar") {
    metaInf {
        this.include("gradle-plugins")
    }
}

publishing {
    repositories {
        maven("https://mymavenrepo.com/repo/2adloi83ZbzFqdzrVebr/")
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = project.group.toString()
            artifactId = "stylus-gradle-plugin"
            version = project.version.toString()

            from(components["kotlin"])
        }
    }
}

gradlePlugin {
    website.set("https://gitlab.com/mt-desktop-libs/stylus-gradle-plugin")
    vcsUrl.set("https://gitlab.com/mt-desktop-libs/stylus-gradle-plugin")

    plugins {
        create("stylusPlugin") {
            id = "com.appspot.magtech.stylus-gradle-plugin"
            implementationClass = "com.appspot.magtech.stylusplugin.StylusPlugin"
            displayName = "Stylus Plugin"
            description = "Plugin for compile .styl files to .css"
            tags.set(listOf("developing", "stylization"))
        }
    }
}

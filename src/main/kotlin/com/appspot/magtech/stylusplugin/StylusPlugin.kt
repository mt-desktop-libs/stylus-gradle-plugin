package com.appspot.magtech.stylusplugin

import org.gradle.api.Plugin
import org.gradle.api.Project
import java.nio.file.*
import java.nio.file.attribute.BasicFileAttributes

class StylusPlugin : Plugin<Project> {

    override fun apply(target: Project) {
        Files.write(Paths.get(target.rootDir.absolutePath).resolve("package.json"),
                listOf("""
            {
                "name": "${target.name}",
                "dependencies": {
                    "stylus": "^0.54.7"
                }
            }
        """.trimIndent()))
        target.tasks.create("npmInstall") {
            it.group = "build"
            target.exec { ex ->
                ex.commandLine("npm", "install")
            }
        }
        target.tasks.create("compileStylus") {
            Files.walkFileTree(Paths.get("src/main/resources/"), object : SimpleFileVisitor<Path>() {
                override fun preVisitDirectory(dir: Path?, attrs: BasicFileAttributes?): FileVisitResult {
                    if (dir != null) {
                        val relativePath = dir.drop(3).map { it.toString() }
                        val result = Paths.get(relativePath.firstOrNull() ?: "", *relativePath.drop(1).toTypedArray())
                        val outPath = Paths.get(target.buildDir.absolutePath).resolve("resources/main").resolve(result)
                        target.exec {
                            it.commandLine(
                                    "./node_modules/.bin/stylus",
                                    dir.toString(),
                                    "-o",
                                    outPath.toString()
                            )
                        }
                    }
                    return super.preVisitDirectory(dir, attrs)
                }
            })
            it.mustRunAfter("processResources")
        }

        target.tasks.named("build") {
            it.dependsOn("npmInstall")
        }
    }
}